/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap;

import edu.umd.cs.treemap.MapLayout;
import edu.umd.cs.treemap.MapModel;
import edu.umd.cs.treemap.Mappable;
import edu.umd.cs.treemap.Rect;

public abstract class AbstractMapLayout
implements MapLayout {
    public static final int VERTICAL = 0;
    public static final int HORIZONTAL = 1;
    public static final int ASCENDING = 0;
    public static final int DESCENDING = 1;

    public abstract void layout(Mappable[] var1, Rect var2);

    public void layout(MapModel model, Rect bounds) {
        this.layout(model.getItems(), bounds);
    }

    public static double totalSize(Mappable[] items) {
        return AbstractMapLayout.totalSize(items, 0, items.length - 1);
    }

    public static double totalSize(Mappable[] items, int start, int end) {
        double sum = 0.0;
        for (int i = start; i <= end; ++i) {
            sum += items[i].getSize();
        }
        return sum;
    }

    public Mappable[] sortDescending(Mappable[] items) {
        Mappable[] s = new Mappable[items.length];
        System.arraycopy(items, 0, s, 0, items.length);
        int n = s.length;
        boolean outOfOrder = true;
        while (outOfOrder) {
            outOfOrder = false;
            for (int i = 0; i < n - 1; ++i) {
                boolean wrong;
                boolean bl = wrong = s[i].getSize() < s[i + 1].getSize();
                if (!wrong) continue;
                Mappable temp = s[i];
                s[i] = s[i + 1];
                s[i + 1] = temp;
                outOfOrder = true;
            }
        }
        return s;
    }

    public static void sliceLayout(Mappable[] items, int start, int end, Rect bounds, int orientation) {
        AbstractMapLayout.sliceLayout(items, start, end, bounds, orientation, 0);
    }

    public static void sliceLayout(Mappable[] items, int start, int end, Rect bounds, int orientation, int order) {
        double total = AbstractMapLayout.totalSize(items, start, end);
        double a = 0.0;
        boolean vertical = orientation == 0;
        for (int i = start; i <= end; ++i) {
            Rect r = new Rect();
            double b = items[i].getSize() / total;
            if (vertical) {
                r.x = bounds.x;
                r.w = bounds.w;
                r.y = order == 0 ? bounds.y + bounds.h * a : bounds.y + bounds.h * (1.0 - a - b);
                r.h = bounds.h * b;
            } else {
                r.x = order == 0 ? bounds.x + bounds.w * a : bounds.x + bounds.w * (1.0 - a - b);
                r.w = bounds.w * b;
                r.y = bounds.y;
                r.h = bounds.h;
            }
            items[i].setBounds(r);
            a += b;
        }
    }
}

