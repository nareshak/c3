/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap;

import edu.umd.cs.treemap.AbstractMapLayout;
import edu.umd.cs.treemap.Mappable;
import edu.umd.cs.treemap.Rect;

public class SliceLayout
extends AbstractMapLayout {
    public static final int BEST = 2;
    public static final int ALTERNATE = 3;
    private int orientation;

    public SliceLayout() {
        this(3);
    }

    public SliceLayout(int orientation) {
        this.orientation = orientation;
    }

    public void layout(Mappable[] items, Rect bounds) {
        if (items.length == 0) {
            return;
        }
        int o = this.orientation;
        if (o == 2) {
            SliceLayout.layoutBest(items, 0, items.length - 1, bounds);
        } else if (o == 3) {
            SliceLayout.layout(items, bounds, items[0].getDepth() % 2);
        } else {
            SliceLayout.layout(items, bounds, o);
        }
    }

    public static void layoutBest(Mappable[] items, int start, int end, Rect bounds) {
        SliceLayout.sliceLayout(items, start, end, bounds, bounds.w > bounds.h ? 1 : 0, 0);
    }

    public static void layoutBest(Mappable[] items, int start, int end, Rect bounds, int order) {
        SliceLayout.sliceLayout(items, start, end, bounds, bounds.w > bounds.h ? 1 : 0, order);
    }

    public static void layout(Mappable[] items, Rect bounds, int orientation) {
        SliceLayout.sliceLayout(items, 0, items.length - 1, bounds, orientation);
    }

    public String getName() {
        return "Slice-and-dice";
    }

    public String getDescription() {
        return "This is the original treemap algorithm, which has excellent stability properies but leads to high aspect ratios.";
    }
}

