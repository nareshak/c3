/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap;

import edu.umd.cs.treemap.Mappable;

public interface MapModel {
    public Mappable[] getItems();
}

