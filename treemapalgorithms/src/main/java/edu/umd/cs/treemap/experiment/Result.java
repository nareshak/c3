/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap.experiment;

class Result {
    double aspect;
    double change;
    double readability;
    String crlf = "\r\n";

    Result() {
        this(0.0, 0.0, 0.0);
    }

    Result(double aspect, double change, double readability) {
        this.aspect = aspect;
        this.change = change;
        this.readability = readability;
    }

    public String toString() {
        return "aspect=" + this.aspect + this.crlf + "change=" + this.change + this.crlf + "readability=" + this.readability + this.crlf;
    }
}

