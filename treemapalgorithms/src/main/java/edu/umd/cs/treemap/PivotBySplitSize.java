/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap;

import edu.umd.cs.treemap.MapLayout;
import edu.umd.cs.treemap.MapModel;
import edu.umd.cs.treemap.OrderedTreemap;
import edu.umd.cs.treemap.Rect;

public class PivotBySplitSize
implements MapLayout {
    OrderedTreemap orderedTreemap = new OrderedTreemap();

    public PivotBySplitSize() {
        this.orderedTreemap.setPivotType(2);
    }

    public void layout(MapModel model, Rect bounds) {
        this.orderedTreemap.layout(model, bounds);
    }

    public String getName() {
        return "Pivot by Split Size";
    }

    public String getDescription() {
        return "Pivot by Split Size";
    }
}

