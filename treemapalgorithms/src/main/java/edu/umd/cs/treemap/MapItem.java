/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap;

import edu.umd.cs.treemap.Mappable;
import edu.umd.cs.treemap.Rect;

public class MapItem
implements Mappable {
    double size;
    Rect bounds;
    int order = 0;
    int depth;

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public int getDepth() {
        return this.depth;
    }

    public MapItem() {
        this(1.0, 0);
    }

    public MapItem(double size, int order) {
        this.size = size;
        this.order = order;
        this.bounds = new Rect();
    }

    public double getSize() {
        return this.size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public Rect getBounds() {
        return this.bounds;
    }

    public void setBounds(Rect bounds) {
        this.bounds = bounds;
    }

    public void setBounds(double x, double y, double w, double h) {
        this.bounds.setRect(x, y, w, h);
    }

    public int getOrder() {
        return this.order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}

