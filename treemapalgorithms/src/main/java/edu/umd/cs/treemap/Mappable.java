/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap;

import edu.umd.cs.treemap.Rect;

public interface Mappable {
    public double getSize();

    public void setSize(double var1);

    public Rect getBounds();

    public void setBounds(Rect var1);

    public void setBounds(double var1, double var3, double var5, double var7);

    public int getOrder();

    public void setOrder(int var1);

    public int getDepth();

    public void setDepth(int var1);
}

