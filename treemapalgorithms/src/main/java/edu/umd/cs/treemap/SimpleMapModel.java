/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap;

import edu.umd.cs.treemap.MapModel;
import edu.umd.cs.treemap.Mappable;
import edu.umd.cs.treemap.Rect;

public class SimpleMapModel
implements MapModel {
    private Mappable[] items;
    private Rect bounds;

    public SimpleMapModel() {
    }

    public SimpleMapModel(Mappable[] items, Rect bounds) {
        this.items = items;
        this.bounds = bounds;
    }

    public void setBounds(Rect bounds) {
        this.bounds = bounds;
    }

    public Rect getBounds() {
        return this.bounds;
    }

    public Mappable[] getItems() {
        return this.items;
    }

    public void setItems(Mappable[] items) {
        this.items = items;
    }
}

