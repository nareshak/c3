/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap;

import edu.umd.cs.treemap.MapLayout;
import edu.umd.cs.treemap.MapModel;
import edu.umd.cs.treemap.Mappable;
import edu.umd.cs.treemap.Rect;

public class StripTreemap
implements MapLayout {
    public static final boolean DEBUG = false;
    Mappable[] items;
    Rect layoutBox;
    boolean lookahead = true;

    public String getName() {
        return "StripTreemap";
    }

    public String getDescription() {
        return "An Ordered Squarified Treemap";
    }

    public void setLookahead(boolean lookahead) {
        this.lookahead = lookahead;
    }

    public void layout(MapModel model, Rect bounds) {
        int i;
        this.items = model.getItems();
        this.layoutBox = bounds;
        double totalSize = 0.0;
        for (i = 0; i < this.items.length; ++i) {
            totalSize += this.items[i].getSize();
        }
        double area = this.layoutBox.w * this.layoutBox.h;
        double scaleFactor = Math.sqrt(area / totalSize);
        int numItems = 0;
        double prevAR = 0.0;
        double ar = 0.0;
        double yoffset = 0.0;
        Rect box = new Rect(this.layoutBox);
        box.x /= scaleFactor;
        box.y /= scaleFactor;
        box.w /= scaleFactor;
        box.h /= scaleFactor;
        for (int finishedIndex = 0; finishedIndex < this.items.length; finishedIndex += numItems) {
            this.debug("A: finishedIndex = " + finishedIndex);
            numItems = this.layoutStrip(box, finishedIndex);
            if (this.lookahead && finishedIndex + numItems < this.items.length) {
                int numItems2 = this.layoutStrip(box, finishedIndex + numItems);
                double ar2a = this.computeAverageAspectRatio(finishedIndex, numItems + numItems2);
                this.computeHorizontalBoxLayout(box, finishedIndex, numItems + numItems2);
                double ar2b = this.computeAverageAspectRatio(finishedIndex, numItems + numItems2);
                this.debug("F: numItems2 = " + numItems2 + ", ar2a=" + ar2a + ", ar2b=" + ar2b);
                if (ar2b < ar2a) {
                    this.debug("G: numItems = " + (numItems += numItems2));
                } else {
                    this.computeHorizontalBoxLayout(box, finishedIndex, numItems);
                    this.debug("H: backup numItems = " + numItems);
                }
            }
            for (i = finishedIndex; i < finishedIndex + numItems; ++i) {
                this.items[i].getBounds().y += yoffset;
            }
            double height = this.items[finishedIndex].getBounds().h;
            yoffset += height;
            box.y += height;
            box.h -= height;
        }
        for (i = 0; i < this.items.length; ++i) {
            Rect rect = this.items[i].getBounds();
            rect.x *= scaleFactor;
            rect.y *= scaleFactor;
            rect.w *= scaleFactor;
            rect.h *= scaleFactor;
            rect.x += bounds.x;
            rect.y += bounds.y;
            this.items[i].setBounds(rect);
        }
    }

    protected int layoutStrip(Rect box, int index) {
        double prevAR;
        double height;
        int numItems = 0;
        double ar = Double.MAX_VALUE;
        do {
            prevAR = ar;
            height = this.computeHorizontalBoxLayout(box, index, ++numItems);
            ar = this.computeAverageAspectRatio(index, numItems);
            this.debug("L.1: numItems=" + numItems + ", prevAR=" + prevAR + ", ar=" + ar);
        } while (ar < prevAR && index + numItems < this.items.length);
        if (ar >= prevAR) {
            height = this.computeHorizontalBoxLayout(box, index, --numItems);
            ar = this.computeAverageAspectRatio(index, numItems);
            this.debug("L.2: backup: numItems=" + numItems);
        }
        return numItems;
    }

    protected double computeHorizontalBoxLayout(Rect box, int index, int numItems) {
        double totalSize = this.computeSize(index, numItems);
        double height = totalSize / box.w;
        double x = 0.0;
        for (int i = 0; i < numItems; ++i) {
            double width = this.items[i + index].getSize() / height;
            this.items[i + index].setBounds(x, 0.0, width, height);
            x += width;
        }
        return height;
    }

    public void debug(String str) {
    }

    double computeSize(int index, int num) {
        double size = 0.0;
        for (int i = 0; i < num; ++i) {
            size += this.items[i + index].getSize();
        }
        return size;
    }

    double computeAverageAspectRatio(int index, int numItems) {
        double tar = 0.0;
        for (int i = 0; i < numItems; ++i) {
            double w = this.items[i + index].getBounds().w;
            double h = this.items[i + index].getBounds().h;
            double ar = Math.max(w / h, h / w);
            tar += ar;
        }
        return tar /= (double)numItems;
    }

    double computeAspectRatio(int index) {
        double w = this.items[index].getBounds().w;
        double h = this.items[index].getBounds().h;
        double ar = Math.max(w / h, h / w);
        return ar;
    }
}

