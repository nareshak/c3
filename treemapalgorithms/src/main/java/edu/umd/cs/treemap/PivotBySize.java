/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap;

import edu.umd.cs.treemap.MapLayout;
import edu.umd.cs.treemap.MapModel;
import edu.umd.cs.treemap.OrderedTreemap;
import edu.umd.cs.treemap.Rect;

public class PivotBySize
implements MapLayout {
    OrderedTreemap orderedTreemap = new OrderedTreemap();

    public PivotBySize() {
        this.orderedTreemap.setPivotType(3);
    }

    public void layout(MapModel model, Rect bounds) {
        this.orderedTreemap.layout(model, bounds);
    }

    public String getName() {
        return "Pivot by Size / Ben B.";
    }

    public String getDescription() {
        return "Pivot by Size, with stopping conditions added by Ben Bederson.";
    }
}

