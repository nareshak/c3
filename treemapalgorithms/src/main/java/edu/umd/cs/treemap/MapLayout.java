/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap;

import edu.umd.cs.treemap.MapModel;
import edu.umd.cs.treemap.Rect;

public interface MapLayout {
    public void layout(MapModel var1, Rect var2);

    public String getName();

    public String getDescription();
}

