/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap.test;

import edu.umd.cs.treemap.MapLayout;
import edu.umd.cs.treemap.MapModel;
import edu.umd.cs.treemap.Mappable;
import edu.umd.cs.treemap.Rect;
import edu.umd.cs.treemap.StripTreemap;
import edu.umd.cs.treemap.test.RandomMap;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class TreemapTest
extends Frame {
    MapModel map;
    MapLayout algorithm;

    public TreemapTest() {
        int w = 400;
        int h = 400;
        this.map = new RandomMap(20);
        Mappable[] items = this.map.getItems();
        this.algorithm = new StripTreemap();
        this.algorithm.layout(this.map, new Rect(0.0, 0.0, w, h));
        this.setBounds(100, 100, w, h);
        this.setVisible(true);
        this.addWindowListener(new WindowAdapter(){

            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }

    public void paint(Graphics g) {
        Mappable[] items = this.map.getItems();
        g.setColor(Color.black);
        for (int i = 0; i < items.length; ++i) {
            Rect rect = items[i].getBounds();
            int a = (int)rect.x;
            int b = (int)rect.y;
            int c = (int)(rect.x + rect.w) - a;
            int d = (int)(rect.y + rect.h) - b;
            g.drawRect(a, b, c, d);
        }
    }

    public static void main(String[] args) {
        new TreemapTest();
    }

}

